import React, { Component } from 'react';

import classes from './items.module.css';
import Card from '../../UI/Card/Card'
import Aux from '../../../hoc/Aux'

class Items extends Component {
    state = {
        courses: [
            {
                courseName: "UX Design",
                courseTitle: "Wednesday Holmes: Art as an Act of Survival",
                courseInstructor: "Jonathan Doe",
                CoursePublishedOn: "54 mins ago",
                view: "512",
                heart: "113",

            },
            {
                courseName: "Graphic Design",
                courseTitle: "21 Delightful Ideas About Graphics Design",
                courseInstructor: "Jonathan Doe",
                CoursePublishedOn: "02 hrs ago",
                view: "512",
                heart: "113",
            }
        ]
    }

    render() {
        return (
            <Aux className={classes.Courses}>
                <ul className={classes.cards}>
                    {this.state.courses.map((e, i) => <li> <Card info={e} key={i} /></li>)}
                </ul>
            </Aux>
        )
    }
}

export default Items;
