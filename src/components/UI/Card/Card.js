import React from 'react';
import classes from './Card.module.css'
import FavoriteIcon from '@material-ui/icons/Favorite';
import VisibilityIcon from '@material-ui/icons/Visibility';
import share from '../../../assets/images/share.png'
import BookmarkIcon from '@material-ui/icons/Bookmark';


const card = (props) => (
    <div className={classes.container}>
        <div className={classes.box} ></div>
        <div className={classes.stacktop} style={{ background: "white" }}>

            <div style={{ marginLeft: "28px ", marginRight: "16px" }}>
                <p className={classes.courseName}>{props.info.courseName}</p>
                <p className={classes.courseTitle}> {props.info.courseTitle}</p>
                <hr className={classes.line} />
                <p className={classes.courseInstructor}>By {props.info.courseInstructor}</p>
                <p className={classes.CoursePublishedOn}>By {props.info.CoursePublishedOn}</p>

                <div className={classes.reactions}>
                    <div style={{ marginTop: "5px " }}>
                        <VisibilityIcon fontSize="inherit" />
                    </div>
                    <div className={classes.view}>{props.info.view}</div>
                    <div style={{ marginLeft: "1%", marginTop: "5px " }}>
                        <FavoriteIcon fontSize="inherit" />
                    </div>
                    <div className={classes.view}>{props.info.heart}</div>
                    <div style={{ marginLeft: "50%", marginTop: "5px " }}>
                        <BookmarkIcon fontSize="inherit" />
                    </div>
                    <div style={{ marginLeft: "1%", marginTop: "5px " }}>

                        <img src={share} height="18px" width="18px" />
                    </div>
                </div>
            </div>

        </div>

    </div>
)
export default card