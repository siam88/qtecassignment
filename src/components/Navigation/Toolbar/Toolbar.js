import React from 'react';

import classes from './Toolbar.module.css';
import Logo from '../../Logo/Logo';
import SearchIcon from '@material-ui/icons/Search';

const toolbar = (props) => (

    <headers className={classes.Toolbar}>
        <div className={classes.Logo}>
            <Logo />
        </div>
        <div className={classes.searchContainer}>
            <table className={classes.elementContainer}>
                <input type="text" className={classes.search} placeholder="search for articles, blog or any readings…" />

                <div className={classes.searchIcon} >
                    <SearchIcon fontSize="small" />
                </div>
            </table>
        </div>
        <div className={classes.authButtons}>
            <button className={classes.AuthButton}>LOGIN</button>|
            <button className={classes.AuthButton}>SIGNUP</button>
        </div>

        <div className={classes.specialButton}>
            <button>PUBLISH</button>
        </div>
    </headers>

)

export default toolbar