import React from 'react';

import CompanyLogo from '../../assets/images/comany-logo.png';
import classes from './Logo.module.css';

const logo = (props) => (
    <div className={classes.Logo} style={{ height: props.height }}>
        <img src={CompanyLogo} alt="Company Logo" />
    </div>
);

export default logo;