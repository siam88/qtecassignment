import './App.css';
import Items from './components/containers/Items/Items';
import Layout from './hoc/Layout/Layout'

function App() {
  return (
    <div>
      <Layout>
        <Items />
      </Layout>
    </div>
  );
}

export default App;
